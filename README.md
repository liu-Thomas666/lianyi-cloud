# 涟漪云 v0.2.0

#### 介绍

蓝奏云列表及解析程序，自由操作蓝奏云内文件（夹），并可解析直链下载等等。  
演示：[查看演示](https://lz.ly93.cc)，讨论交流联系QQ：29397395

#### 更新说明

1. 修复url_fix函数bug导致解析失败
2. 新增本地文件缓存支持（当config中redis为false时自动转用本地缓存）
3. 过期缓存自动清理，防止占用空间
4. 批量删除、移动文件(夹)使用curl_multi异步处理


#### 大体功能如下

1. 浏览任意目录内文件（夹）
2. 批量移动文件
3. 批量删除文件（夹）
4. 重命名文件夹
5. 新建文件夹
6. 文件夹加密、描述及修改
7. 文件直链  
	①文件id + 文件名形式：域名/id/文件名  
	②文件id 形式：域名/id  
	③文件分享id + 文件名形式：域名/文件分享id?pwd=分享密码（分享密码选填）  
	④文件分享id形式：域名/文件分享id/文件名?pwd=分享密码（分享密码选填）
8. 文件批量上传

#### 软件架构

1. PHP >= 5.6 (开启redis扩展)
2. Redis

#### 安装教程

1. 下载源码
2. 将源码上传至你的网站根目录并解压
3. 获取cookie(浏览器F12控制台执行)：
	```javascript
	if(!/(^|\.)woozooo\.com$/i.test(document.location.host))
		throw new Error('请登录到蓝奏云控制台在执行此代码！');
	
	var regex = /(?<=^|;)\s*([^=]+)=\s*(.*?)\s*(?=;|$)/g,
		cookies = {},re;
	while(re = regex.exec(document.cookie))
		if(re[1] === 'ylogin'||re[1] === 'phpdisk_info')
			cookies[re[1]] = re[1]+'='+re[2]+';';
	
	if(!cookies.hasOwnProperty('phpdisk_info'))
		throw new Error('获取cookie失败，请确认您已登录到蓝奏云控制台！');
	
	(function (str) {
		var oInput = document.createElement('input');
		oInput.value = str;
		document.body.appendChild(oInput);
		oInput.select();
		document.execCommand("Copy");
		oInput.remove();
		alert('复制成功');
	})(Object.values(cookies).join(' '));
	```
4. 修改配置文件(config.php)相关配置，尤其将步骤3获取的（phpdisk_info=xxx; ylogin=yyy;）xxx、yyy填写到对应区域
5. 配置伪静态  
	Nginx：
	```nginx
	if (!-e $request_filename) {
		rewrite ^/([1-9]\d*|i[a-zA-Z0-9]+)(\.[\w]+|/([^/]+))?$ /api.php?id=$1&name=$3 break;
	}
	```
	Apache：
	```apache
	RewriteEngine On

	RewriteCond %{REQUEST_FILENAME} !-d
	RewriteCond %{REQUEST_FILENAME} !-f
	RewriteRule ^([1-9]\d*|i[a-zA-Z0-9]+)(\.\w+|/([^/]+))?$ /api.php?id=$1&name=$3 [QSA,L]
	```